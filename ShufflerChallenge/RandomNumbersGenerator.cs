﻿using System;
using System.IO;
using System.Linq;
using ShufflerChallenge.ExtensionMethods;

namespace ShufflerChallenge
{
    /// <summary>
    /// Generates a range of numbers in a random sequence.
    /// </summary>
    static class RandomNumbersGenerator
    {
        private const int MinNumber = 1;
        private const int MaxNumber = 10000;

        /// <summary>
        /// Creates an array of integers, within a range, in a random order.
        /// </summary>
        /// <returns>Array of integers in a random order.</returns>
        private static int[] CreateShuffledArray()
        {
            int[] numbers = Enumerable.Range(MinNumber, MaxNumber).ToArray();
            numbers.Randomize();
            return numbers;
        }

        /// <summary>
        /// Prints numbers, within a specified range, in a random order to a text file.
        /// The text file can be found in bin/RandomizedNumberList of the project directory.
        /// </summary>
        private static void Main()
        {
            // Setup the output directory
            string binDirectoryPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string outputFolderPath = Path.Combine(binDirectoryPath, "RandomizedNumberList");
            Directory.CreateDirectory(outputFolderPath);
            string randomNumberListFileName = "\\randomNumbers.txt";
            File.Delete(outputFolderPath + randomNumberListFileName);

            // Write to file
            int[] randomizedNumbers = CreateShuffledArray();
            File.AppendAllLines(outputFolderPath + randomNumberListFileName, randomizedNumbers.Select(i => i.ToString()).ToArray());
            Console.WriteLine("Random number list written to: " + outputFolderPath + randomNumberListFileName);
        }
    }
}
