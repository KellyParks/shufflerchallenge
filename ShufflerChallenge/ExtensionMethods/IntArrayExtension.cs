﻿using System;

namespace ShufflerChallenge.ExtensionMethods
{
    /// <summary>
    /// Adds a method that randomizes the order of elements in an array of integers.
    /// </summary>
    public static class IntArrayExtension
    {
        /// <summary>
        /// Randomizes the order of elements in the array of integers.
        /// </summary>
        /// <param name="numbers">Current instance of the array of integers.</param>
        public static void Randomize(this int[] numbers)
        {
            Random randomizer = new Random();
            //loop from the end of the array to the beginning of the array until we hit index 0
            for (int index = numbers.Length - 1; index > 0; index--)
            {
                int randomIndex = randomizer.Next(0, index + 1);
                Swap(numbers, index, randomIndex);
            }
        }

        /// <summary>
        /// Switches the values found at two indexes in a collection of integers.
        /// </summary>
        /// <param name="numbers">The array of integers to swap the values in.</param>
        /// <param name="firstIndex">Index of the element to swap with the element found at secondIndex</param>
        /// <param name="secondIndex">Index of the element to swap with the element found at firstIndex</param>
        private static void Swap(int[] numbers, int firstIndex, int secondIndex)
        {
            int tempValue = numbers[secondIndex];
            numbers[secondIndex] = numbers[firstIndex];
            numbers[firstIndex] = tempValue;
        }
    }
}
