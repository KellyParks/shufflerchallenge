# To Run the Program
This is a .NET console application. To run it, open the program in Visual Studio 2019 by opening the `ShufflerChallenge/ShufflerChallenge.sln` file. Then click the ShufflerChallenge button to compile and run the application. The list of random numbers can be found at `ShufflerChallenge/bin/RandomizedNumberList` from the project root.

# To Run the Tests
In Visual Studio, right-click **UnitTests** and select **Run Tests**.

# Rationale
For this challenge I wanted to build a solution that would shuffle elements in a collection, similar to shuffling cards in a deck.
My primary concerns for this solution were:

* choosing the best data structure to store the numbers 1-10000
* not outputting duplicate numbers
* making the shuffle as random as possible
* choosing an efficient randomizer method
* minding the time and space complexity of the algorithm

Using an array with the Knuth Shuffle algorithm seems to address all these concerns.

## How the Knuth Shuffle Works
The Knuth Shuffle randomizes the order of a collection of elements, while also preventing an element from being shuffled more than once. It does this by iterating backwards over a collection, such as an array, then choosing a random index from the elements that haven't yet been shuffled. Then it swaps the value of the element at the random index with the value at the loop's index.

### Time Complexity
The algorithm contains a loop that iterates over almost the entire array, but doesn't search for a particular value. It swaps (assigns) values as it goes through the array, instead of inserting/moving/deleting elements, so it has a time complexity of O(n) (linear).

### Space Complexity
The algorithm only swaps two values in-place in the original array variable, so the space complexity is O(1) (constant).

## Design Decisions
The algorithm doesn't make use of pointers and deletion/insertion methods, it just switches two values in-place, so I thought using a simple array to store all the numbers was fine. 

A function to randomize elements in a collection felt like a good candidate for as an extension method. It sounds like an operation that would be useful for various instances of arrays. Additionally, extension methods have the added benefit of enforcing the Open/Closed Principle.

RandomNumbersGenerator serves as the entry point of the program. Following the Single Responsibility Principle, it's only purpose is to print a list of numbers to a text file.