using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShufflerChallenge.ExtensionMethods;
using System;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class IntArrayExtensionsTests
    {
        [TestMethod]
        public void IntArrayExtension_Randomize_ShouldRandomizeOrderEachTime()
        {
            // Setup
            int[] collection1 = Enumerable.Range(1, 10000).ToArray();
            int[] collection2 = Enumerable.Range(1, 10000).ToArray();

            // Execute
            collection1.Randomize();
            collection2.Randomize();

            // Assert
            CollectionAssert.AreNotEqual(collection1, collection2);
        }

        [TestMethod]
        public void IntArrayExtension_Randomize_ShouldContainFullRange()
        {
            // Setup
            int[] sequentialCollection = Enumerable.Range(1, 10000).ToArray();

            // Execute
            int[] collectionUnderTest = Enumerable.Range(1, 10000).ToArray();
            collectionUnderTest.Randomize();

            // Assert
            CollectionAssert.AreNotEqual(collectionUnderTest, sequentialCollection);

            // Sort the randomized collection, then compaire it to the 
            // sequential collection to ensure no numbers are missing
            // or duplicated. I thought this was cleaner and more efficient
            // than writing a for-loop in a test
            Array.Sort(collectionUnderTest);
            CollectionAssert.AreEqual(collectionUnderTest, sequentialCollection);
        }
    }
}
